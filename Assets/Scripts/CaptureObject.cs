﻿using UnityEngine;

public class CaptureObject : MonoBehaviour
{
	#region PublicFields

	public EAnimationType AnimationType
	{
		get
		{
			return m_animationType;
		}
		set
		{
			m_animationType = value;
		}
	}

	public AnimationClip SourceClip
	{
		get
		{
			return m_sourceClip;
		}
		set
		{
			m_sourceClip = value;
		}
	}

	public GameObject ObjectToFollow
	{
		get
		{
			return m_objectToFollow;
		}
		set
		{
			m_objectToFollow = value;
		}
	}

	#endregion

	#region SerializeFields

	[SerializeField] private EAnimationType m_animationType;
	[SerializeField] private AnimationClip m_sourceClip;
	[SerializeField] private GameObject m_objectToFollow;

	#endregion
}
