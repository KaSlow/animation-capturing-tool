﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class AnimationCaptureHelper : MonoBehaviour
{
	#region PublicFields

	public string ClipName
	{
		get
		{
			return m_fileName;
		}
		set
		{
			m_fileName = value;
		}
	}

	#endregion

	#region SerializeFields

	[SerializeField] private EAnimationType m_animationType;

	[Tooltip("If null, it will be taken from corresponding capture object")]
	[SerializeField]
	private AnimationClip m_sourceClip;

	[SerializeField] private int m_framesPerSecond = 30;
	[SerializeField] private Vector2Int m_cellSize = new Vector2Int(100, 100);
	[SerializeField] private Camera m_captureCamera;
	[SerializeField] private CameraPositioner m_cameraPositioner;

	[Tooltip("If empty, name of the file will be the same as name of the source clip")]
	[SerializeField] private string m_fileName;

	[SerializeField] private int m_spriteSheetCount;
	[SerializeField] private string m_spriteSheetFolderName;
	[SerializeField] private string m_animationClipsPath;

	#endregion

	#region PrivateFields

	[SerializeField] private List<CaptureObject> m_captureObjects;

	private GameObject m_target;
	private int m_currentFrame;
	private float m_animationLenght;

	#endregion

	#region PublicMethods

	public void CaptureAnimation(int captureIndex = 0)
	{
		FindTargetModel();
		StartCoroutine(CaptureAnimation(SaveCapture, captureIndex));
	}

	public void ClearProperties()
	{
		m_sourceClip = null;
		m_target = null;
		m_cameraPositioner.FollowTarget = null;
	}

	#endregion

	#region PrivateMethods

	private void FindTargetModel()
	{
		foreach (CaptureObject capturedObject in m_captureObjects)
		{
			capturedObject.gameObject.SetActive(false);
		}

		CaptureObject objectToCapture = m_captureObjects.Find(c => c.AnimationType == m_animationType);
		m_target = objectToCapture.gameObject;

		if (m_sourceClip == null)
		{
			m_sourceClip = objectToCapture.SourceClip;
		}

		if (m_target != null)
		{
			m_target.SetActive(true);
		}

		if (m_cameraPositioner.FollowTarget == null)
		{
			m_cameraPositioner.FollowTarget = objectToCapture.ObjectToFollow;
			m_cameraPositioner.Initialize();
		}
	}

	private void SampleAnimation(float time)
	{
		if (m_sourceClip == null || m_target == null)
		{
			Debug.LogWarning("SourceClip and Target should be set before sample animation!");
		}
		else
		{
			m_sourceClip.SampleAnimation(m_target, time);
		}
	}

	private IEnumerator CaptureAnimation(Action<Texture2D, Texture2D, int> onComplete, int captureIndex)
	{
		if (m_sourceClip == null || m_target == null)
		{
			Debug.LogWarning("CaptureCamera should be set before capturing animation!");
			yield break;
		}

		m_fileName = m_sourceClip.name;

		if (m_cameraPositioner != null)
		{
			m_cameraPositioner.Capturing = true;
		}

		int numFrames = 0;

		if (m_spriteSheetCount > 0)
		{
			numFrames = m_spriteSheetCount;
			if (m_sourceClip.length > 0)
			{
				m_framesPerSecond = (int)(m_spriteSheetCount / m_sourceClip.length);
			}
		}
		else
		{
			numFrames = (int)(m_sourceClip.length * m_framesPerSecond);
		}

		int gridCellCount = SqrtCeil(numFrames);
		var atlasSize = new Vector2Int(m_cellSize.x * gridCellCount, m_cellSize.y * gridCellCount);
		var atlasPos = new Vector2Int(0, atlasSize.y - m_cellSize.y);

		var diffuseMap = new Texture2D(atlasSize.x, atlasSize.y, TextureFormat.ARGB32, false)
		{
			filterMode = FilterMode.Point
		};
		ClearAtlas(diffuseMap, Color.clear);

		var normalMap = new Texture2D(atlasSize.x, atlasSize.y, TextureFormat.ARGB32, false)
		{
			filterMode = FilterMode.Point
		};
		ClearAtlas(normalMap, new Color(0.5f, 0.5f, 1.0f, 0.0f));

		var rtFrame = new RenderTexture(m_cellSize.x, m_cellSize.y, 24, RenderTextureFormat.ARGB32)
		{
			filterMode = FilterMode.Point,
			antiAliasing = 1,
			hideFlags = HideFlags.HideAndDontSave
		};

		Shader normalCaptureShader = Shader.Find("Hidden/ViewSpaceNormal");

		m_captureCamera.targetTexture = rtFrame;
		Color cachedCameraColor = m_captureCamera.backgroundColor;

		try
		{
			for (m_currentFrame = 0; m_currentFrame < numFrames; m_currentFrame++)
			{
				float currentTime = m_currentFrame / (float)numFrames * m_sourceClip.length;
				SampleAnimation(currentTime);
				yield return null;

				m_captureCamera.backgroundColor = Color.clear;
				m_captureCamera.Render();
				Graphics.SetRenderTarget(rtFrame);
				diffuseMap.ReadPixels(new Rect(0, 0, rtFrame.width, rtFrame.height), atlasPos.x, atlasPos.y);
				diffuseMap.Apply();

				m_captureCamera.backgroundColor = new Color(0.5f, 0.5f, 1.0f, 0.0f);
				m_captureCamera.RenderWithShader(normalCaptureShader, "");
				Graphics.SetRenderTarget(rtFrame);
				normalMap.ReadPixels(new Rect(0, 0, rtFrame.width, rtFrame.height), atlasPos.x, atlasPos.y);
				normalMap.Apply();

				atlasPos.x += m_cellSize.x;

				if ((m_currentFrame + 1) % gridCellCount == 0)
				{
					atlasPos.x = 0;
					atlasPos.y -= m_cellSize.y;
				}
			}

			onComplete.Invoke(diffuseMap, normalMap, captureIndex);
		}
		finally
		{
			Graphics.SetRenderTarget(null);
			m_captureCamera.targetTexture = null;
			m_captureCamera.backgroundColor = cachedCameraColor;
			DestroyImmediate(rtFrame);
		}
	}

	private int SqrtCeil(int input)
	{
		return Mathf.CeilToInt(Mathf.Sqrt(input));
	}

	private void ClearAtlas(Texture2D texture, Color color)
	{
		var pixels = new Color[texture.width * texture.height];
		for (int i = 0; i < pixels.Length; i++)
		{
			pixels[i] = color;
		}

		texture.SetPixels(pixels);
		texture.Apply();
	}

	private void SaveCapture(Texture2D diffuseMap, Texture2D normalMap, int captureIndex)
	{
		string diffusePath = $"{Application.dataPath}/{m_spriteSheetFolderName}/{m_fileName}_{captureIndex}.png";

		if (string.IsNullOrEmpty(diffusePath))
		{
			return;
		}

		string directory = Path.GetDirectoryName(diffusePath);
		string normalPath = $"{Application.dataPath}/{m_spriteSheetFolderName}/{m_fileName}_{captureIndex}NormalMap.png";

		File.WriteAllBytes(diffusePath, diffuseMap.EncodeToPNG());
		File.WriteAllBytes(normalPath, normalMap.EncodeToPNG());

		AssetDatabase.Refresh();

		Sprite sp = Sprite.Create(
			diffuseMap,
			new Rect(0, 0, diffuseMap.width, diffuseMap.height),
			new Vector2(0, 0),
			30);

		string relativePath = "";
		if (diffusePath.StartsWith(Application.dataPath))
		{
			relativePath = "Assets" + diffusePath.Substring(Application.dataPath.Length);
		}

		sp.name = "New_sprite";
		AssetDatabase.AddObjectToAsset(sp, relativePath);
		AssetDatabase.SaveAssets();

		string path = AssetDatabase.GetAssetPath(sp);
		var textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;

		if (textureImporter != null)
		{
			textureImporter.textureType = TextureImporterType.Sprite;
			textureImporter.spritePixelsPerUnit = 100;
			textureImporter.spriteImportMode = SpriteImportMode.Multiple;
			textureImporter.spritesheet = SliceSprite(diffuseMap, $"{m_fileName}_{captureIndex}");
			EditorUtility.SetDirty(textureImporter);
			textureImporter.SaveAndReimport();
		}

		List<Sprite> spriteList = AssetDatabase.LoadAllAssetsAtPath(path)
			.OfType<Sprite>()
			.ToList();
		spriteList.Remove(spriteList.First());
		Sprite[] sprites = spriteList.ToArray();

		SaveAnimationClip(sprites, $"{m_fileName}_{captureIndex}");
		AssetDatabase.ImportAsset(path);

		if (m_cameraPositioner != null)
		{
			m_cameraPositioner.Capturing = false;
		}
	}

	private SpriteMetaData[] SliceSprite(Texture texture, string fileName)
	{
		var newData = new List<SpriteMetaData>();

		int sliceWidth = m_cellSize.x;
		int sliceHeight = m_cellSize.y;
		int spriteIndex = 0;

		for (int i = texture.height; i > 0; i -= sliceHeight)
		{
			for (int j = 0; j < texture.width; j += sliceWidth)
			{
				if (spriteIndex >= m_currentFrame)
				{
					continue;
				}

				var smd = new SpriteMetaData();
				smd.pivot = new Vector2(0.5f, 0.5f);
				smd.alignment = 9;
				smd.name = $"{fileName}_{spriteIndex}";
				smd.rect = new Rect(j, i - sliceHeight, sliceWidth, sliceHeight);
				spriteIndex++;
				newData.Add(smd);
			}
		}

		return newData.ToArray();
	}

	private void SaveAnimationClip(IReadOnlyList<Sprite> sprites, string fileName)
	{
		var animClip = new AnimationClip { frameRate = m_framesPerSecond };
		var spriteBinding = new EditorCurveBinding { type = typeof(SpriteRenderer), path = "", propertyName = "m_Sprite" };
		var spriteKeyFrames = new ObjectReferenceKeyframe[sprites.Count];
		m_animationLenght = m_sourceClip.length;
		float keyTime = m_animationLenght / m_currentFrame;

		for (int i = 0; i < sprites.Count; i++)
		{
			spriteKeyFrames[i] = new ObjectReferenceKeyframe { time = i * keyTime, value = sprites[i] };
		}

		AnimationUtility.SetObjectReferenceCurve(animClip, spriteBinding, spriteKeyFrames);

		AssetDatabase.CreateAsset(animClip, $"{m_animationClipsPath}/{fileName}_animation.anim");
		AssetDatabase.SaveAssets();
		AssetDatabase.Refresh();
	}

	#endregion
}
