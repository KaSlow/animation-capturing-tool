﻿using System.Collections;
using UnityEditor;
using UnityEngine;

public class AutomaticCapture : MonoBehaviour
{
	#region SerializeFields

	[SerializeField] private AnimationCaptureHelper m_animationCaptureHelper;
	[SerializeField] private CameraPositioner m_cameraPositioner;
	[SerializeField] private int m_capturesAmount;

	#endregion

	#region PublicMethods

	public void CaptureAnimations()
	{
		StartCoroutine(CaptureAnimation());
	}

	#endregion

	#region PrivateMethods

	private IEnumerator CaptureAnimation()
	{
		for (int i = 0; i < m_capturesAmount; i++)
		{
			m_animationCaptureHelper.CaptureAnimation(i);

			while (m_cameraPositioner.Capturing)
			{
				yield return new WaitForEndOfFrame();
			}

			m_cameraPositioner.Rotate();
		}

		m_animationCaptureHelper.ClearProperties();

		EditorUtility.DisplayDialog(
			"Capture completed successfully",
			m_animationCaptureHelper.ClipName + " animation was captured successfully",
			"YEAH!");

		Debug.Log("Capture completed successfully");
	}

	#endregion
}
