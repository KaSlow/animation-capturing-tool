﻿using UnityEngine;

public class CameraPositioner : MonoBehaviour
{
	#region PublicFields

	public bool Capturing { get; set; }
	public GameObject FollowTarget { get; set; }

	#endregion

	#region SerializeFields

	[SerializeField] private Camera m_captureCamera;
	[SerializeField] [Range(0, 360)] private float m_rotationAngle;

	#endregion

	#region PrivateFields

	private Vector3 m_offset;

	#endregion

	#region UnityMethods

	private void Update()
	{
		if (Capturing)
		{
			m_captureCamera.transform.position = FollowTarget.transform.position + m_offset;
			m_captureCamera.transform.LookAt(FollowTarget.transform);
		}
	}

	#endregion

	#region PublicMethods

	public void Initialize()
	{
		m_offset = m_captureCamera.transform.position - FollowTarget.transform.position;
	}

	public void Rotate()
	{
		m_captureCamera.transform.RotateAround(FollowTarget.transform.position, FollowTarget.transform.up, m_rotationAngle);
		m_offset = m_captureCamera.transform.position - FollowTarget.transform.position;
	}

	#endregion
}
