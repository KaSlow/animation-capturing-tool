﻿public enum EAnimationType
{
	Idle,
	Running,
	InwardSlash,
	OutwardSlash,
	Dodge
}
